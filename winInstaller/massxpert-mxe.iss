[Setup]
AppName=massXpert

#define public winerootdir "z:"
; Set version number below
#define public version "6.0.3"
AppVersion={#version}

#define public arch "mingw64"
#define public platform "win7+"
#define sourceDir "z:/home/rusconi/devel/massxpert/development/"
#define cmakeBuildDir "z:/home/rusconi/devel/massxpert/build-area/mxe/"
#define outputDir "z:/home/rusconi/devel/massxpert/development/winInstaller"
#define mxeDllDir "z:/home/rusconi/devel/mxe/mxe-dlls"

; Set version number below
AppVerName=massXpert version {#version}
DefaultDirName={commonpf}\massXpert
DefaultGroupName=massXpert
OutputDir={#outputDir}

; Set version number below
OutputBaseFilename=massXpert-{#arch}-{#platform}-v{#version}-setup

; Set version number below
OutputManifestFile=massXpert-{#arch}-{#platform}-v{#version}-setup-manifest.txt
ArchitecturesAllowed=x64
ArchitecturesInstallIn64BitMode=x64

LicenseFile="{#sourceDir}\LICENSE"
AppCopyright="Copyright (C) 2016-2022 Filippo Rusconi"

AllowNoIcons=yes
AlwaysShowComponentsList=yes
AllowRootDirectory=no
AllowCancelDuringInstall=yes
AppComments="massXpert, by Filippo Rusconi"
AppContact="Filippo Rusconi, PhD, Staff researcher at CNRS, France"
CloseApplications=yes
CreateUninstallRegKey=yes
DirExistsWarning=yes
WindowResizable=yes
WizardImageFile="{#sourceDir}\images\splashscreen-massxpert-innosetup.bmp"
WizardImageStretch=yes

[Dirs]
Name: "{app}\data"
Name: "{app}\doc"

[Files]

Source: "{#mxeDllDir}/*.dll"; DestDir: {app}; Flags: ignoreversion recursesubdirs;

Source: "{#sourceDir}/doc/history.html"; DestDir: {app}\doc;

Source: "{#cmakeBuildDir}/src/massXpert.exe"; DestDir: {app};

Source: "{#sourceDir}/data/*"; DestDir: {app}\data; Flags: recursesubdirs;

Source: "{#sourceDir}/doc/user-manual/massxpert-doc.pdf"; DestDir: {app}\doc;
Source: "{#sourceDir}/doc/user-manual/build/user-manual/html/user-manual/*"; Flags: recursesubdirs; DestDir: {app}\doc\html;

[Icons]
Name: "{group}\massXpert"; Filename: "{app}\massXpert.exe"; WorkingDir: "{app}"
Name: "{group}\Uninstall massXpert"; Filename: "{uninstallexe}"

[Run]
Filename: "{app}\massXpert.exe"; Description: "Launch massXpert"; Flags: postinstall nowait unchecked

