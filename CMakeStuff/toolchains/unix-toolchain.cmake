message("UNIX non APPLE environment")
message("Please run the configuration like this:")
message("cmake -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Debug -DBUILD_USER_MANUAL=1 ../development")

set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES /usr/include)
set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES /usr/include)

# This is used throughout all the build system files
set(TARGET massxpert)

# Now that we know what is the TARGET (in the toolchain files above,
# we can compute the lowercase TARGET (used for string replacements in 
# configure files and also for the resource compilation with windres.exe.
string(TOLOWER ${TARGET} TARGET_LOWERCASE)
message("TARGET_LOWERCASE: ${TARGET_LOWERCASE}")

## INSTALL directories
if(NOT CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX /usr)
endif()
set(BIN_DIR ${CMAKE_INSTALL_PREFIX}/bin)
set(DATA_DIR ${CMAKE_INSTALL_PREFIX}/share/${TARGET}/data)
set(DOC_DIR ${CMAKE_INSTALL_PREFIX}/share/doc/${TARGET})


# The appstream, desktop and icon files
install(FILES org.msxpertsuite.massxpert.desktop
  DESTINATION ${CMAKE_INSTALL_PREFIX}/share/applications)

install(FILES org.msxpertsuite.massxpert.appdata.xml
  DESTINATION ${CMAKE_INSTALL_PREFIX}/share/metainfo)

install(FILES images/icons/16x16/massxpert.png
  DESTINATION
  ${CMAKE_INSTALL_PREFIX}/share/icons/hicolor/16x16/apps)

install(FILES images/icons/32x32/massxpert.png
  DESTINATION
  ${CMAKE_INSTALL_PREFIX}/share/icons/hicolor/32x32/apps)

install(FILES images/icons/48x48/massxpert.png
  DESTINATION
  ${CMAKE_INSTALL_PREFIX}/share/icons/hicolor/48x48/apps)

install(FILES images/icons/64x64/massxpert.png
  DESTINATION
  ${CMAKE_INSTALL_PREFIX}/share/icons/hicolor/64x64/apps)


## Platform-dependent compiler flags:
include(CheckCXXCompilerFlag)

if (WITH_FPIC)
  add_definitions(-fPIC)
endif()

