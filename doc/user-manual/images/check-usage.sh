#!/bin/sh

for i in *.png
do 
    grep $i ../*.tex > /dev/null 2>&1
    if [ "$?" != 0 ]
    then 
	echo "file $i not used" 
    fi
done
