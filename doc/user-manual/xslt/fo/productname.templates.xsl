<?xml version="1.0" encoding="UTF-8"?>


<!--Purpose:-->
<!--Make sure the productname element is used to output the line-->
<!--<productname> is part of the msXpertSuite software package-->

	<!DOCTYPE xsl:stylesheet
	[
	<!ENTITY % fonts SYSTEM "fonts.ent">
	<!ENTITY % colors SYSTEM "colors.ent">
	<!ENTITY % metrics SYSTEM "metrics.ent">
	%fonts;
	%colors;
	%metrics;
	]>
	<xsl:stylesheet exclude-result-prefixes="d"
		version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:d="http://docbook.org/ns/docbook"
		xmlns:exsl="http://exslt.org/common" 
		xmlns:fo="http://www.w3.org/1999/XSL/Format">


		<xsl:template name="book.info.productname" mode="book.titlepage.productname.info">
			<xsl:param name="productname"/>

			<!--We want to make sure the line about the fact that <productname/> is part-->
			<!--of the msXpertSuite software project.-->

			<!--As of 20190102, the line says: -->
			<!--"$productname is part of the msXpertSuite software package"-->
			<!--productname should be either "massXpert" or "mineXpert".-->

			<fo:block font-weight="bold" font-size="&x-large;" text-align="center">
				<xsl:value-of select="$productname"/> is part
				of the msXpertSuite software package </fo:block>

			<fo:block font-weight="bold" font-size="&large;" text-align="center">	Modelling,
				simulating and analyzing ionized flying species</fo:block>

		</xsl:template>

	</xsl:stylesheet>
