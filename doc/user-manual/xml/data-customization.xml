<?xml version='1.0' encoding='utf-8' ?>
<!DOCTYPE chapter [

<!ENTITY % entities SYSTEM "entities.ent">
%entities;
]>

<chapter
	xml:id="chap_data-customizations"
	xmlns="http://docbook.org/ns/docbook" version="5.0"
	xmlns:xlink="http://www.w3.org/1999/xlink">

	<info>

		<title>Data Customizations</title>

		<keywordset>
			<keyword>polymer chemistry definition</keyword>
			<keyword>customization</keyword>
		</keywordset>

	</info>

	<para> In this chapter, the user will be walked trough an example of how new
		polymer chemistry definition data can be generated and included in the
		automatic <quote>data detection system</quote> of &massXp; (that is how new polymer
		chemistry definitions should be registered with the system).</para>

	<para> Customization is typically performed by the normal user (not the
		Administrator nor the Root of the machine) and as such new data are
		typically stored in the user's <quote>home</quote> directory. On
		<productname>UNIX</productname> machines, the <quote>home</quote> directory
		is usually the <filename class="directory">/home/username</filename>
		directory, where username is the login username. On
		<productname>MS-Windows</productname>, that directory is typically the
		<filename class="directory">C:/Documents and Settings/username</filename>
		once again with username being the login username.  </para>

	<tip>

		<para>Although <productname>MS-Windows</productname> pathnames use a
			back&nbsp;slash (<quote>\</quote>, in this book these are composed using
			forward&nbsp;slashes for a number of valid reasons.  The reader only needs
			to replace back&nbsp;slashes with the forward variety.  </para>

	</tip>

	<para> In the next sections we will refer to that <quote>home
			directory</quote> (be it on <productname>UNIX</productname> or
		<productname>MS-Windows</productname> machines) as the $HOME directory, as
		this the standard environment variable describing that directory in
		<productname>GNU/Linux</productname>.</para>

	<para> When &massXp; is executed, it automatically tries to read data
		configuration files from the home directory (in the <filename
			class="directory">.massxpert</filename> directory). Once this is done, it
		reads all the data configuration files in the installation directory
		(typically, on <productname>GNU/Linux</productname> that would be the
		configuration data in the <filename
			class="directory">/usr/share/msxpertsuite-massxpert</filename> directory or, on
		<productname>MS-Windows</productname>, the <filename
			class="directory">c:/Program Files/msxpertsuite/massxpert</filename> directory).</para>

	<para> We said above that &massXp; tries to read the data configuration files
		from the home directory. But upon its very first execution, right after
		installation, that directory does not exist, and in fact &massXp; creates
		that directory for us to populate it some day with interesting new
		data.</para>

	<para> The <filename class="directory">$HOME/.massxpert</filename> directory
		should have a structure mimicking the one that was created upon installation
		of the software, that is, it should contain the following two directories:
	</para>

	<itemizedlist>
		<listitem>
			<para>

				<filename class="directory">polChemDefs</filename>

			</para>
		</listitem>
		<listitem>
			<para>

				<filename class="directory">polSeqs</filename>

			</para>
		</listitem>
	</itemizedlist>

	<para>Those are the directories where the user is invited to store their
		personal data. In order to start a new definition, one might simply copy in
		the <filename class="directory">polChemDefs</filename> one of the polymer
		chemistry definitions that are shipped with &massXp;. What should be copied?
		An entire polymer chemistry definition directory, like for example the
		following:</para>

	<para> <filename
			class="directory">/usr/local/share/massxpert/polChemDefs/protein-1-letter</filename></para>

	<para>or</para>

	<para> <filename class="directory">C:/Program
			Files/msXpertSuite/data/massxpert/polChemDefs/protein-1-letter</filename></para>

	<para> Once that polymer chemistry definition is copied, one may start
		studying how it actually works. This directory contains the following kinds
		of files:</para>

	<itemizedlist>
		<listitem>
			<para>

				<filename>protein-1-letter.xml</filename>: the polymer chemistry
				definition file. This is the file that is read upon selection of the
				corresponding polymer chemistry definition name in &xpd;. If the polymer
				chemistry definition is not yet registered with the system (described
				later), then open that file by browsing to it by clicking
				<guilabel>Cancel</guilabel> (see <xref linkend="chap_xpertdef-module"/>);

			</para>
		</listitem>
		<listitem>
			<para>

				<acronym>SVG</acronym> files: <emphasis>scalar vector
					graphics</emphasis> files used to render graphically the sequence in
				the sequence editor. For example, <filename>arginine.svg</filename>
				contains the graphical representation of the arginine monomer. There are
				such graphics files also for the modifications (like, for example, the
				<filename>sulphation.svg</filename> contains the graphical
				representation of the sulphation modification.  <xref
					linkend="fig_pol-chem-defs-directory-protein-and-saccharide"/> shows
				two examples of <acronym>SVG</acronym> files belonging to two distinct
				polymer chemistry definitions;

			</para>
		</listitem>
		<listitem>
			<para>

				<filename>chemPad.conf</filename>: configuration file for the chemical pad in
				the &xpc; module;

			</para>
		</listitem>
		<listitem>
			<para>

				<filename>monomer_dictionary</filename>: file establishing the relationship
				between any monomer code of the polymer chemistry definition and the graphical
				<acronym>SVG</acronym> file to be used to render graphically that monomer in the
				sequence editor;

			</para>
		</listitem>
		<listitem>
			<para>

				<filename>modification_dictionary</filename>: file establishing the
				relationship between any monomer modification (see <xref
					linkend="sec_selected-monomer-modification"/>) and the graphical
				<acronym>SVG</acronym> file to be used to render graphically that
				modification onto the modified monomer in the sequence editor;

			</para>
		</listitem>
		<listitem>
			<para>

				<filename>cross_linker_dictionary</filename>: file establishing the
				relationship between any cross-link (see <xref
					linkend="sec_monomer-cross-linking"/>) and the graphical
				<acronym>SVG</acronym> file to be used to render graphically that
				cross-link onto the cross-linked monomers in the sequence editor;

			</para>
		</listitem>
		<listitem>
			<para>

				<filename>pka_ph_pi.xml</filename>: file describing the acido-basic data
				(see <xref linkend="sec_pka-ph-pi-and-charges"/>) pertaining to
				ionizable chemical groups in the different entities of the polymer
				chemistry definition;

			</para>
		</listitem>
	</itemizedlist>


	<figure xml:id="fig_pol-chem-defs-directory-protein-and-saccharide">

		<title>The polymer chemistry definition directory</title>

		<mediaobject>

			<imageobject role="fo">
				<imagedata fileref="print-pol-chem-defs-directory-protein-and-saccharide.png" format="PNG" scale="100"/>
			</imageobject>
			<imageobject role="html">
				<imagedata fileref="web-pol-chem-defs-directory-protein-and-saccharide.png" format="PNG" scale="80"/>
			</imageobject>

			<caption>

				<para>Each monomer of the polymer chemistry definition ought to have a
					corresponding <acronym>SVG</acronym> file with which it has to be rendered
					graphically should that monomer be inserted in the polymer sequence. This
					example shows two <acronym>SVG</acronym> files corresponding to two monomers each
					belonging to a different polymer chemistry definition.  </para>

			</caption>

		</mediaobject>

	</figure>

	<para> The polymer sequence editor is not a classical editor. There is no font
		in this editor: when the user starts keying-in a polymer sequence in the
		editor, the small <acronym>SVG</acronym> graphics files are rendered into
		raster <emphasis>vignettes</emphasis> at both the proper resolution and
		screen size and displayed in the sequence editor. The user is totally in
		charge of designing the <acronym>SVG</acronym> graphics files for each of
		the monomers defined in the polymer sequence editor. Of course, reusing
		material is perfectly possible.</para>

	<tip>

		<para> One powerful software to edit <acronym>SVG</acronym> files is <link
				xlink:href="https://inkscape.org/">Inkscape</link>, on any
			platform.</para>

	</tip>

	<para>There is one constraint: that the
		<filename>monomer_dictionary</filename> file lists with precision
		<quote>what monomer code goes with what <acronym>SVG</acronym> graphics
			file</quote>. That file has the following contents, for example, for the
		<quote>protein-1-letter</quote> polymer chemistry definition, as shipped
		in the &massXp; package:</para>

	<literallayout>
		# This file is part of the massXpert project.

		# The "massXpert" project is released ---in its entirety--- under the
		# GNU General Public License and was started (in the form of the GNU
		# polyxmass project) at the Centre National de la Recherche
		# Scientifique (FRANCE), that granted me the formal authorization to
		# publish it under this Free Software License.

		# Copyright (C) 2006,2007 Filippo Rusconi

		# This is the monomer_dictionary file where the correspondences
		# between the codes of each monomer and their graphic file (pixmap
		# file called "image") used to graphicallly render them in the
		# sequence editor are made.

		# The format of the file is like this :
		# -------------------------------------

		# A%alanine.svg

		# where A is the monomer code and alanine.svg is a
		# resolution-independent svg file.

		# Each line starting with a '#' character is a comment and is ignored
		# during parsing of this file.

		# This file is case-sensitive.

		A%alanine.svg
		C%cysteine.svg
		D%aspartate.svg
		E%glutamate.svg
		F%phenylalanine.svg
		G%glycine.svg
		H%histidine.svg
		I%isoleucine.svg
		K%lysine.svg
		L%leucine.svg
		M%methionine.svg
		N%asparagine.svg
		P%proline.svg
		Q%glutamine.svg
		R%arginine.svg
		S%serine.svg
		T%threonine.svg
		V%valine.svg
		W%tryptophan.svg
		Y%tyrosine.svg
	</literallayout>


	<para> What one sees from the contents of the file is that each monomer code
		has an associated <acronym>SVG</acronym> file. For example, when the user
		has to key-in a valine monomer, they key-in the code <keycap>V</keycap> and
		&xpe; knows that the monomer vignette to show has to be rendered using the
		<filename>valine.svg</filename> file.</para>

	<para> For the monomer modification graphical rendering, the situation is
		somewhat different, as seen in the
		<filename>modification_dictionary</filename> file:</para>


	<literallayout>
		# This file is part of the massXpert project.

		# The "massXpert" project is released ---in its entirety--- under the
		# GNU General Public License and was started (in the form of the GNU
		# polyxmass project) at the Centre National de la Recherche
		# Scientifique (FRANCE), that granted me the formal authorization to
		# publish it under this Free Software License.

		# Copyright (C) 2006,2007 Filippo Rusconi

		# This is the modification_dictionary file where the correspondences
		# between the name of each modification and their graphic file (pixmap
		# file called "image") used to graphicallly render them in the
		# sequence editor are made. Also, the graphical operation that is to
		# be performed upon chemical modification of a monomer is listed ('T'
		# for transparent and 'O' for opaque). See the manual for details.

		# The format of the file is like this :
		# -------------------------------------

		# Phosphorylation%T%phospho.svg

		# where Phosphorylation is the name of the modification. T indicates
		# that the visual rendering of the modification is a transparent
		# process (O indicates that the visual rendering of the modification
		# is a full image replacement 'O' like opaque). phospho.svg is a
		# resolution-independent svg file.


		# Each line starting with a '#' character is a comment and is ignored
		# during parsing of this file. 

		# This file is case-sensitive.

		Phosphorylation%T%phospho.svg
		Sulphation%T%sulpho.svg
		AmidationAsp%O%asparagine.svg
		Acetylation%T%acetyl.svg
		AmidationGlu%O%glutamine.svg
		Oxidation%T%oxidation.svg
	</literallayout>

	<para> There are two ways to render a chemical modification of a monomer:
	</para>

	<itemizedlist>
		<listitem>
			<para>

				<emphasis>Opaque</emphasis> rendering: the initial monomer vignette is
				replaced using the one listed in the file for the modification. One
				example is the <quote>AmidationGlu</quote> modification:</para>

			<para>AmidationGlu%O%glutamine.svg</para> 

			<para>Upon amidation of a glutamyl residue (<quote>AmidationGlu</quote>
				is the name of a modification in the current polymer chemistry
				definition), the graphical representation of the modification involves
				the <emphasis>replacement</emphasis> of the glutamyl residue vignette
				in the sequence editor with the new one, that happens to be in the
				<filename>glutamine.svg</filename> file. In other words, the process
				involves an <quote><emphasis>O</emphasis>paque</quote> overlay of the
				vignette for unmodified Glu with a vignette rendered by using the
				<filename>glutamine.svg</filename> file.

			</para>
		</listitem>
		<listitem>
			<para>

				<emphasis>Transparent</emphasis> rendering: the initial monomer vignette
				is overlaid with a new vignette that is read from an
				<acronym>SVG</acronym> file that has a transparent background. One
				example is the <quote>Phosphorylation</quote> modification:</para>

			<para>Phosphorylation%T%phospho.svg</para>

			<para>The monomer undergoing a phosphorylation has its vignette overlaid
				with a <quote><emphasis>T</emphasis>ransparent</quote> one showing a
				small red <quote>P</quote> that is read from the
				<filename>phospho.svg</filename> file.

			</para>
		</listitem>
	</itemizedlist>


	<para> When designing vignettes, the best thing to do is to convert the text
		to path, so that the rendering is absolutely perfect.</para>

	<warning>

		<para>It is absolutely essential, for the proper working of the sequence
			editor, that the <acronym>SVG</acronym> files be square (that is, width =
			height).  </para>

	</warning>

	<para> Once the new polymer chemistry has been correctly defined, it is time to
		register that new definition to the system. To recap: all the files for that
		definition should reside in a same directory, exactly the same way as the
		files pertaining to a given polymer chemistry definition are shipped in
		&massXp; altogether in one directory. The name of the new polymer chemistry
		definition should be unambiguous, with respect to other registered polymer
		chemistry definitions.</para>

	<para> The way personal polymer chemistry definitions are registered is by
		creating a personal polymer chemistry definition catalogue file, which
		must comply with both following requirements:</para>

	<itemizedlist>
		<listitem>
			<para>

				Be named <filename>xxxxx-polChemDefsCat</filename>, with
				<quote>xxxxx</quote> being a discretionary string (this might well be
				your login username). The requirement is that
				<emphasis><filename>-polChemDefsCat</filename></emphasis> be the last
				part of the filename.</para>

			<tip>

				<para>Please <emphasis>DO NOT USE</emphasis> spaces or diacritical
					signs in your filenames.  <emphasis>RESTRICT</emphasis> yourself
					to ASCII characters between [a-z], [0-9], <quote>_</quote> and
					<quote>-</quote>.</para>


				<para>This is actually something very general as a recommendation in
					order to not suffer from severe headaches when you least expect
					them&hellip;</para>

			</tip>

		</listitem>
		<listitem>
			<para>

				Be located in the <filename
					class="directory">$HOME/.massxpert/polChemDefs</filename> directory
				and have the following format:</para>

			<para>dna=/path/to/definition/directory/dna/dna.xml</para>

			<para> In this example, the <quote>dna</quote> polymer chemistry
				definition is being registered as a file <filename>dna.xml</filename>
				located in the <filename class="directory">dna</filename> directory,
				itself located in the <filename
					class="directory">/path/to/definition/directory</filename>
				directory;

			</para>
		</listitem>
	</itemizedlist>


	<para> Note that if a new polymer chemistry definition should be made
		available system-wide, then it is logical that its directory be placed along
		the ones shipped with &massXp; and a new local catalogue file might be
		created to register the new polymer chemistry definition.</para>

	<para> At this point the new polymer chemistry definition might be tested.
		Typically, that involves restarting the &massXp; program and creating a
		brand new polymer sequence of the new definition type. The first step is to
		check if the new definition is successfully registered with the system, that
		is, it should show up a an available definition upon creation of the new
		polymer sequence. If not, then that means that the catalogue file could not
		be found or parsed correctly. </para>

	<para> When problems like this one occurs, the first thing to do is to ensure
		that the console window (on <productname>MS-Windows</productname> it is
		systematically started along with the program; on
		<productname>GNU/Linux</productname> the way to have it is to start the
		program from the shell) so as to look with attention at the different
		messages that might help understanding what is failing.</para>

	<para> Please, do not hesitate to submit bug reports (see <xref
			linkend="sec_preface-feedback-from-users"/> for the method  to contect
		the author for feature requests or bug reports).</para>

</chapter>

