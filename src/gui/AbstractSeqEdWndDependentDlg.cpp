/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes


/////////////////////// Local includes
#include "AbstractSeqEdWndDependentDlg.hpp"

namespace msxps
{

	namespace massxpert
	{



AbstractSeqEdWndDependentDlg::AbstractSeqEdWndDependentDlg(
  SequenceEditorWnd *editorWnd,
  Polymer *polymer,
  PolChemDef *polChemDef,
  const QString &configSettingsFilePath,
  const QString &wndTypeName,
  const QString &wndTitle)
  : QDialog{static_cast<QWidget *>(editorWnd)},
    mp_editorWnd{editorWnd},
    mp_polymer{polymer},
    mp_polChemDef{polChemDef},
    m_configSettingsFilePath{configSettingsFilePath},
    m_wndTypeName{wndTypeName},
    m_wndTitle{wndTitle}
{
  if(!editorWnd)
    qFatal("Fatal error at %s@%d. Program aborted.", __FILE__, __LINE__);

  // We cannot test for non-0 pointers because the subclasses may not use both
  // pointers or any of them. The only that has to be there and non-0 is the
  // pointer to the sequence editor window, which is the rationale of this
  // class.
}


AbstractSeqEdWndDependentDlg::~AbstractSeqEdWndDependentDlg()
{
}


void
AbstractSeqEdWndDependentDlg::closeEvent([[maybe_unused]] QCloseEvent *event)
{
  // No real close, because we did not ask that
  // close==destruction. Thus we only hide the dialog remembering its
  // position and size.

  writeSettings();
}


void
AbstractSeqEdWndDependentDlg::writeSettings()
{
  QSettings settings(m_configSettingsFilePath, QSettings::IniFormat);

  settings.beginGroup(m_wndTypeName);
  settings.setValue("geometry", saveGeometry());
  settings.endGroup();
}


void
AbstractSeqEdWndDependentDlg::readSettings()
{
  QSettings settings(m_configSettingsFilePath, QSettings::IniFormat);

  settings.beginGroup(m_wndTypeName);
  restoreGeometry(settings.value("geometry").toByteArray());
  settings.endGroup();
}


SequenceEditorWnd *
AbstractSeqEdWndDependentDlg::editorWnd() const
{
  return mp_editorWnd;
}

} // namespace massxpert

} // namespace msxps
