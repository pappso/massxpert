# Get the name of the current directory but with no path:
get_filename_component(TARGET ${CMAKE_CURRENT_BINARY_DIR} NAME)

message("")
message(STATUS "${BoldGreen}Start processing ${TARGET} dir.${ColourReset}")
message("")


########################################################
# Files

file(GLOB massgui_SRCS *.cpp)
file(GLOB massgui_UIS ui/*.ui)
file(GLOB Resources *.qrc)

# On MINGW, the QCustomPlot files are added to the project,
# while on GNU/Linux, there is a library. See main CMakeLists.txt.
if(APPLE OR (SYSTEM_UNAME_S MATCHES "^MINGW.*"))

	file(GLOB ${TARGET}_qcustomplot_SRC ${QCUSTOMPLOT_SRC_DIR}/*.cpp)

endif()


set(CMAKE_INCLUDE_CURRENT_DIR ON)

# Instruct CMake to run moc automatically when needed.
set(CMAKE_AUTOMOC ON)

find_package(Qt5 COMPONENTS Widgets Xml Svg Sql Script)

qt5_wrap_ui(massgui_UIS_H ${massgui_UIS})

### CMake now only automocs the generated files and not the source files.
set_source_files_properties(${massgui_UIS_H} PROPERTIES SKIP_AUTOMOC ON)


###############################################################
# Configuration of the binary to be built
###############################################################

add_library(massgui STATIC 
	${massgui_SRCS}
	${massgui_UIS_H}
	${glob_SRCS}
	${${TARGET}_qcustomplot_SRC}
	)


qt5_use_modules(massgui Widgets)
qt5_use_modules(massgui Xml)
qt5_use_modules(massgui Svg)
qt5_use_modules(massgui Sql)
qt5_use_modules(massgui Script)
qt5_use_modules(massgui PrintSupport)

include_directories(
	# For the config.h file generated from config.h.in	
	${CMAKE_BINARY_DIR}

	# For all the source subdirectories
	${CMAKE_SOURCE_DIR}

	# For the ui_Xyyyyy.h files
	${CMAKE_CURRENT_BINARY_DIR} 

	${CMAKE_CURRENT_SOURCE_DIR}
	${CMAKE_CURRENT_BINARY_DIR}


	${Qt5Widgets_INCLUDE_DIRS}
	${Qt5Xml_INCLUDE_DIRS}
	${Qt5Svg_INCLUDE_DIRS}
	${Qt5Sql_INCLUDE_DIRS}
	${Qt5Script_INCLUDE_DIRS}
	${Qt5PrintSupport_INCLUDE_DIRS})


# On GNU/Linux, make sure we have the QCustomPlot library
# installed on the system (on MINGW, we have the
# source files (see above).
if((NOT SYSTEM_UNAME_S MATCHES "^MINGW.*") AND
	(NOT SYSTEM_UNAME_S MATCHES "Darwin"))

	find_package(QCustomPlot)

	if(NOT QCustomPlot_FOUND)
		message(FATAL_ERROR "QCustomPlot not found!")
	else()
		message(STATUS "QCustomPlot found!")
	endif()

endif()

if(APPLE)
	set(MASSLIB "${CMAKE_BINARY_DIR}/libmass/libmass.a")
else()
	set(MASSLIB "-Wl,-whole-archive ${CMAKE_BINARY_DIR}/libmass/libmass.a -Wl,-no-whole-archive")
endif()

# Use the Widgets module from Qt 5 and the libmass. We craft a variable for this
# so as to transmit it directly to the tests subdirectory that will need it.

set(required_target_link_libraries 
	${MASSLIB} 
	Qt5::Widgets 
	Qt5::Xml 
	Qt5::Svg 
	Qt5::Sql 
	Qt5::Script
	Qt5::PrintSupport)

# When in UNIX but not APPLE, we can make use the system-wide qcustomplot library.
if(UNIX AND NOT APPLE)
	set(required_target_link_libraries
		${required_target_link_libraries}
		"qcustomplot")
endif()

# on MINGW.*, we'll need to add the -lstdc++ flag. Don't ask why.
if(SYSTEM_UNAME_S MATCHES "^MINGW.*")
	set(required_target_link_libraries
		${required_target_link_libraries}
		"stdc++")
endif()


#### Debugging stuff
#message(STATUS required_target_link_libraries: ${required_target_link_libraries})
#get_property(dirs DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY INCLUDE_DIRECTORIES)
#foreach(dir ${dirs})
#message(STATUS "included dir='${dir}'")
#endforeach()


# Finally actually set the liking dependencies to the library.
target_link_libraries(massgui
	${required_target_link_libraries})


message(STATUS "${TARGET} CMAKE_CURRENT_BINARY_DIR: ${CMAKE_CURRENT_BINARY_DIR}")
message(STATUS "Qt5Widgets: " ${Qt5Widgets_INCLUDE_DIRS})
message(STATUS "Qt5Xml: " ${Qt5Xml_INCLUDE_DIRS})
message(STATUS "Qt5Svg: " ${Qt5Svg_INCLUDE_DIRS})
message(STATUS "Qt5Sql: " ${Qt5Sql_INCLUDE_DIRS})
message(STATUS "Qt5Script: " ${Qt5Script_INCLUDE_DIRS})
message(STATUS "Qt5PrintSupport: " ${Qt5PrintSupport_INCLUDE_DIRS}) 
if(PROFILE)
	message(STATUS "Profiling is requested, adding linker -pg flag.")
endif()


message("")
message(STATUS "${BoldGreen}Finished processing ${TARGET} dir.${ColourReset}")
message("")


