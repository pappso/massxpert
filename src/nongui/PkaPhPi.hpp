/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef PKA_PH_PI_HPP
#define PKA_PH_PI_HPP


/////////////////////// Qt includes
#include <QList>


/////////////////////// Local includes
#include "Polymer.hpp"
#include "Monomer.hpp"
#include "Modif.hpp"
#include "ChemicalGroup.hpp"
#include "PropListHolder.hpp"

namespace msxps
{

	namespace massxpert
	{



class PkaPhPi : public PropListHolder
{
  private:
  double m_ph;
  double m_pi;

  const Polymer &m_polymer;
  CalcOptions m_calcOptions;

  double m_positiveCharges;
  double m_negativeCharges;

  // Not allocated locally, but when set, *this takes ownership of
  // these lists, and these lists get fried upon destruction of *this.
  QList<Monomer *> *mpa_monomerList;
  QList<Modif *> *mpa_modifList;

  bool *mp_aborted;
  int *mp_progress;

  public:
  PkaPhPi(Polymer &,
          CalcOptions &,
          QList<Monomer *> * = 0,
          QList<Modif *> *   = 0);

  ~PkaPhPi();

  void setPh(double);
  double ph();

  double pi();
  double positiveCharges();
  double negativeCharges();

  void setCalcOptions(const CalcOptions &);

  void setMonomerList(QList<Monomer *> *);
  void setModifList(QList<Modif *> *);

  int calculateCharges();
  int calculatePi();

  double calculateChargeRatio(double, bool);


  int accountPolymerEndModif(int, const ChemicalGroup &);
  int accountMonomerModif(const Monomer &, ChemicalGroup &);
};

} // namespace massxpert

} // namespace msxps


#endif // PKA_PH_PI_HPP
