/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef COORDINATES_HPP
#define COORDINATES_HPP


/////////////////////// Qt includes
#include <QString>
#include <QList>


namespace msxps
{

	namespace massxpert
	{



class Coordinates
{
  protected:
  int m_start;
  int m_end;

  public:
  Coordinates(int = -1, int = -1);
  Coordinates(const Coordinates &);
  ~Coordinates();

  void setStart(int);
  int start() const;

  void setEnd(int);
  void incrementEnd();
  int end() const;

  int length() const;
  QString indicesAsText() const;
  QString positionsAsText() const;

  bool setFromText(QString);

  void reset();
};


class CoordinateList : public QList<Coordinates *>
{
  protected:
  QString m_comment;

  public:
  CoordinateList(QString = QString(), QList<Coordinates *> * = 0);

  CoordinateList(const CoordinateList &);

  ~CoordinateList();

  CoordinateList &operator=(const CoordinateList &other);

  void setCoordinates(const Coordinates &);
  void setCoordinates(const CoordinateList &);
  void appendCoordinates(const Coordinates &);
  int setCoordinates(const QString &);

  void setComment(QString comment);
  QString comment() const;

  int leftMostCoordinates(QList<int> &) const;
  bool isLeftMostCoordinates(Coordinates *) const;

  int rightMostCoordinates(QList<int> &) const;
  bool isRightMostCoordinates(Coordinates *) const;

  bool encompassIndex(int) const;

  bool overlap() const;

  QString indicesAsText() const;
  QString positionsAsText() const;

  void empty();

  void debugPutStdErr();
};

} // namespace massxpert

} // namespace msxps


#endif // COORDINATES_HPP
