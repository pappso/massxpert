/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef CLEAVER_HPP
#define CLEAVER_HPP


/////////////////////// Local includes
#include "CleaveSpec.hpp"
#include "CleaveOptions.hpp"
#include "CalcOptions.hpp"
#include "Polymer.hpp"
#include "IonizeRule.hpp"
#include "CleaveOligomer.hpp"
#include "OligomerList.hpp"


namespace msxps
{

	namespace massxpert
	{



class Cleaver
{
  private:
  const QPointer<Polymer> mp_polymer;
  const PolChemDef *mp_polChemDef;
  CleaveOptions m_cleaveOptions;
  CalcOptions m_calcOptions;
  IonizeRule m_ionizeRule;

  QList<int> m_cleaveIndexList;
  QList<int> m_noCleaveIndexList;

  // Pointer to an oligomer list which WE DO NOT OWN.
  OligomerList *mp_oligomerList;

  public:
  Cleaver(Polymer *,
          const PolChemDef *,
          const CleaveOptions &,
          const CalcOptions &,
          const IonizeRule &);

  Cleaver(const Cleaver &);
  ~Cleaver();

  void setOligomerList(OligomerList *);
  OligomerList *oligomerList();

  bool cleave(bool = false);
  int cleavePartial(int);

  QString cleaveAgentName() const;
  int analyzeCrossLinks(OligomerList *);
  int analyzeCrossLinkedOligomer(Oligomer *, OligomerList *);

  int fillIndexLists();
  int resolveCleavageNoCleavage();
  int removeDuplicatesCleavage();

  int findCleaveMotif(CleaveMotif &, int, int);
  bool accountCleaveRule(CleaveRule *, CleaveOligomer *);

  void emptyOligomerList();
};

} // namespace massxpert

} // namespace msxps


#endif // CLEAVER_HPP
