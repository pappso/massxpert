/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Local includes
#include "ChemicalGroupRule.hpp"


namespace msxps
{

namespace massxpert
{
	
	ChemicalGroupRule::ChemicalGroupRule(QString entity, QString name, int outcome)
  : m_entity(entity), m_name(name), m_outcome(outcome)
{
  Q_ASSERT(m_outcome == CHEMGROUP_RULE_LOST ||
           m_outcome == CHEMGROUP_RULE_PRESERVED);
}


void
ChemicalGroupRule::setEntity(QString entity)
{
  m_entity = entity;
}


QString
ChemicalGroupRule::entity()
{
  return m_entity;
}


void
ChemicalGroupRule::setName(QString name)
{
  m_name = name;
}


QString
ChemicalGroupRule::name()
{
  return m_name;
}


void
ChemicalGroupRule::setOutcome(int outcome)
{
  m_outcome = outcome;
}


int
ChemicalGroupRule::outcome()
{
  return m_outcome;
}


bool
ChemicalGroupRule::renderXmlElement(const QDomElement &element)
{
  QDomElement child;

  // In an acidobasic definition file, the following xml structure
  // is encountered:

  //
  //
  //       <mnmchemgroup>
  //         <name>N-term NH2</name>
  //	<pka>9.6</pka>
  // 	<acidcharged>TRUE</acidcharged>
  // 	<polrule>left_trapped</polrule>
  // 	<chemgrouprule>
  // 	  <entity>LE_PLM_MODIF</entity>
  // 	  <name>Acetylation</name>
  // 	  <outcome>LOST</outcome>
  // 	</chemgrouprule>
  //

  // The relevant DTD line is:
  // <!ELEMENT chemgrouprule(entity,name,outcome)>

  // And the element the parameter points to is:

  //  <chemgrouprule>

  // Which means that element.tagName() == "chemgrouprule" and that we'll
  // have to go one step down to the first child of the current node
  // in order to get to the <entity> element.

  if(element.tagName() != "chemgrouprule")
    return false;

  child = element.firstChildElement("entity");

  if(child.isNull())
    return false;

  m_entity = child.text();

  child = child.nextSiblingElement();

  if(child.isNull() || child.tagName() != "name")
    return false;

  m_name = child.text();

  child = child.nextSiblingElement();

  if(child.isNull() || child.tagName() != "outcome")
    return false;

  if(child.text() == "LOST")
    m_outcome = CHEMGROUP_RULE_LOST;
  else if(child.text() == "PRESERVED")
    m_outcome = CHEMGROUP_RULE_PRESERVED;
  else
    return false;

  return true;
}

} // namespace massxpert

} // namespace msxps
