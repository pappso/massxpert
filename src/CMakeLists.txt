message(\n${BoldRed}"Now configuring src/ for ${TARGET}"${ColourReset}\n)


########################################################
# Files
set(${TARGET}_nongui_SRCS
  #
  # Some globals.
  nongui/globals.cpp
  #
  # The classes that define the polymer chemistry
  nongui/Atom.cpp
  nongui/AtomCount.cpp
  nongui/ChemicalGroup.cpp
  nongui/ChemicalGroupRule.cpp
  nongui/CleaveMotif.cpp
  nongui/CleaveOligomer.cpp
  nongui/CleaveOptions.cpp
  nongui/CleaveRule.cpp
  nongui/CleaveSpec.cpp
  nongui/Cleaver.cpp
  nongui/CrossLink.cpp
  nongui/CrossLinkList.cpp
  nongui/CrossLinkedRegion.cpp
  nongui/CrossLinker.cpp
  nongui/CrossLinkerSpec.cpp
  nongui/Formula.cpp
  nongui/FragOptions.cpp
  nongui/FragRule.cpp
  nongui/FragSpec.cpp
  nongui/Fragmenter.cpp
  nongui/Ionizable.cpp
  nongui/IonizeRule.cpp
  nongui/Isotope.cpp
  nongui/Modif.cpp
  nongui/ModifSpec.cpp
  nongui/Monomer.cpp
  nongui/MonomerDictionary.cpp
  nongui/MonomerSpec.cpp
  nongui/Oligomer.cpp
  nongui/OligomerList.cpp
  nongui/OligomerPair.cpp
  nongui/PkaPhPi.cpp
  nongui/PkaPhPiDataParser.cpp
  nongui/PolChemDef.cpp
  nongui/PolChemDefCatParser.cpp
  nongui/PolChemDefEntity.cpp
  nongui/PolChemDefSpec.cpp
  nongui/Polymer.cpp
  nongui/Ponderable.cpp
  #
  # The sequence class.
  nongui/Sequence.cpp
  #
  # Utility classes
  nongui/Coordinates.cpp
  nongui/CalcOptions.cpp
  nongui/MassList.cpp
  #
  # Graphical representation of chemical entities
  nongui/ChemEntVignette.cpp
  nongui/ChemEntVignetteRenderer.cpp
  nongui/PeakCentroid.cpp
  #
  # The property system.
  nongui/Prop.cpp
  nongui/PropListHolder.cpp
  #
  # The user/system configuration
  nongui/ConfigSetting.cpp
  nongui/ConfigSettings.cpp
  nongui/UserSpec.cpp
)

set(${TARGET}_gui_SRCS
  main.cpp
  gui/AboutDlg.cpp
  gui/AbstractMainTaskWindow.cpp
  gui/AbstractPolChemDefDependentDlg.cpp
  gui/AbstractSeqEdWndDependentDlg.cpp
  gui/Application.cpp
  gui/AtomDefDlg.cpp
  gui/CalculatorChemPadDlg.cpp
  gui/CalculatorChemPadGroupBox.cpp
  gui/CalculatorRecorderDlg.cpp
  gui/CalculatorWnd.cpp
  gui/ChemPadButton.cpp
  gui/CleavageDlg.cpp
  gui/CleaveOligomerTableView.cpp
  gui/CleaveOligomerTableViewMimeData.cpp
  gui/CleaveOligomerTableViewModel.cpp
  gui/CleaveOligomerTableViewSortProxyModel.cpp
  gui/CleaveSpecDefDlg.cpp
  gui/CompositionTreeView.cpp
  gui/CompositionTreeViewItem.cpp
  gui/CompositionTreeViewModel.cpp
  gui/CompositionTreeViewSortProxyModel.cpp
  gui/CompositionsDlg.cpp
  gui/ConfigSettingsDlg.cpp
  gui/CrossLinkerDefDlg.cpp
  gui/DecimalPlacesOptionsDlg.cpp
  gui/FragSpecDefDlg.cpp
  gui/FragmentOligomerTableView.cpp
  gui/FragmentOligomerTableViewMimeData.cpp
  gui/FragmentOligomerTableViewModel.cpp
  gui/FragmentOligomerTableViewSortProxyModel.cpp
  gui/FragmentationDlg.cpp
  gui/MainWindow.cpp
  gui/MassListSorterDlg.cpp
  gui/MassSearchDlg.cpp
  gui/MassSearchOligomerTableView.cpp
  gui/MassSearchOligomerTableViewMimeData.cpp
  gui/MassSearchOligomerTableViewModel.cpp
  gui/MassSearchOligomerTableViewSortProxyModel.cpp
  gui/ModifDefDlg.cpp
  gui/MonomerCodeEvaluator.cpp
  gui/MonomerCrossLinkDlg.cpp
  gui/MonomerDefDlg.cpp
  gui/MonomerModificationDlg.cpp
  gui/MzCalculationDlg.cpp
  gui/MzCalculationTreeView.cpp
  gui/MzCalculationTreeViewItem.cpp
  gui/MzCalculationTreeViewModel.cpp
  gui/MzCalculationTreeViewSortProxyModel.cpp
  gui/MzLabFormulaBasedActionsDlg.cpp
  gui/MzLabInputOligomerTableView.cpp
  gui/MzLabInputOligomerTableViewDlg.cpp
  gui/MzLabInputOligomerTableViewModel.cpp
  gui/MzLabInputOligomerTableViewSortProxyModel.cpp
  gui/MzLabMassBasedActionsDlg.cpp
  gui/MzLabMatchBasedActionsDlg.cpp
  gui/MzLabOutputOligomerTableView.cpp
  gui/MzLabOutputOligomerTableViewDlg.cpp
  gui/MzLabOutputOligomerTableViewModel.cpp
  gui/MzLabOutputOligomerTableViewSortProxyModel.cpp
  gui/MzLabWnd.cpp
  gui/PkaPhPiDlg.cpp
  gui/PolChemDefWnd.cpp
  gui/PolymerModificationDlg.cpp
  gui/RegionSelection.cpp
  gui/SeqToolsDlg.cpp
  gui/SequenceEditorFindDlg.cpp
  gui/SequenceEditorGraphicsView.cpp
  gui/SequenceEditorGraphicsViewKeySequenceHandling.cpp
  gui/SequenceEditorGraphicsViewKeyboardHandling.cpp
  gui/SequenceEditorWnd.cpp
  gui/SequenceImportDlg.cpp
  gui/SequencePurificationDlg.cpp
  gui/SequenceSelection.cpp
)

set(${TARGET}_UIS
  gui/ui/AboutDlg.ui
  gui/ui/AbstractPolChemDefDependentDlg.ui
  gui/ui/AtomDefDlg.ui
  gui/ui/CalculatorChemPadDlg.ui
  gui/ui/CalculatorRecorderDlg.ui
  gui/ui/CalculatorWnd.ui
  gui/ui/CleavageDlg.ui
  gui/ui/CleaveSpecDefDlg.ui
  gui/ui/CompositionsDlg.ui
  gui/ui/ConfigSettingsDlg.ui
  gui/ui/CrossLinkerDefDlg.ui
  gui/ui/DecimalPlacesOptionsDlg.ui
  gui/ui/FragSpecDefDlg.ui
  gui/ui/FragmentationDlg.ui
  gui/ui/MassSearchDlg.ui
  gui/ui/ModifDefDlg.ui
  gui/ui/MonomerCrossLinkDlg.ui
  gui/ui/MonomerDefDlg.ui
  gui/ui/MonomerModificationDlg.ui
  gui/ui/MzCalculationDlg.ui
  gui/ui/MzLabFormulaBasedActionsDlg.ui
  gui/ui/MzLabInputOligomerTableViewDlg.ui
  gui/ui/MzLabMassBasedActionsDlg.ui
  gui/ui/MzLabMatchBasedActionsDlg.ui
  gui/ui/MzLabOutputOligomerTableViewDlg.ui
  gui/ui/MzLabWnd.ui
  gui/ui/NumeralsLocaleConverterDlg.ui
  gui/ui/PkaPhPiDlg.ui
  gui/ui/PolChemDefWnd.ui
  gui/ui/PolymerModificationDlg.ui
  gui/ui/SequenceEditorFindDlg.ui
  gui/ui/SequenceEditorWnd.ui
  gui/ui/SequenceImportDlg.ui
  gui/ui/SequencePurificationDlg.ui
)

find_package(Qt6 COMPONENTS Widgets Xml Svg SvgWidgets PrintSupport REQUIRED)

qt6_wrap_ui(${TARGET}_UIS_H ${${TARGET}_UIS})

qt6_add_resources(${TARGET}_QRC_CPP ../massxpert.qrc)

# Instruct CMake to run moc automatically when needed.
set(CMAKE_AUTOMOC ON)

### These two lines required because now CMake only automocs
### the generated files and not the source files.
# NOT sure about this. Let's comment that for the moment.
# set_source_files_properties(${${TARGET}_UIS_H} PROPERTIES SKIP_AUTOMOC ON)
# set_source_files_properties(${${TARGET}_QRC_CPP} PROPERTIES SKIP_AUTOMOC ON)

include_directories(

  # The config file is binary directory-specific !!!
  # For the config.h file generated from config.h.in
  # For the ui_Xyyyyy.h files
  ${CMAKE_CURRENT_BINARY_DIR}

  # For all the source subdirectories
  ${CMAKE_SOURCE_DIR}
  ${CMAKE_CURRENT_SOURCE_DIR}
)


###############################################################
# Configuration of the binary to be built
###############################################################

# Only now can we add the executable, because we have defined the sources of the
# object library above.

if(WIN32 OR _WIN32)

  add_executable(${TARGET} WIN32
    ${${TARGET}_nongui_SRCS}
    ${${TARGET}_gui_SRCS}
    ${${TARGET}_UIS_H}
    ${${TARGET}_QRC_CPP}
    ${PLATFORM_SPECIFIC_SOURCES}
  )

elseif(UNIX AND NOT APPLE)

  add_executable(${TARGET}
    ${${TARGET}_nongui_SRCS}
    ${${TARGET}_gui_SRCS}
    ${${TARGET}_UIS_H}
    ${${TARGET}_QRC_CPP}
    ${PLATFORM_SPECIFIC_SOURCES}
  )

elseif(APPLE)

  # Copy the icon file to the Contents/Resources directory of the bundle at
  # location ${MACOSX_PACKAGE_LOCATION}.
  set_source_files_properties(${CMAKE_SOURCE_DIR}/images/icons/${TARGET}.icns 
    PROPERTIES MACOSX_PACKAGE_LOCATION Resources)

  # Identify MacOS bundle

  set(MACOSX_BUNDLE_BUNDLE_NAME ${TARGET})
  message(STATUS "MACOSX_BUNDLE_BUNDLE_NAME: ${MACOSX_BUNDLE_BUNDLE_NAME}")

  set(MACOSX_BUNDLE_BUNDLE_VERSION ${PROJECT_VERSION})
  set(MACOSX_BUNDLE_LONG_VERSION_STRING ${PROJECT_VERSION})
  set(MACOSX_BUNDLE_SHORT_VERSION_STRING ${PROJECT_VERSION})
  set(MACOSX_BUNDLE_COPYRIGHT ${COPYRIGHT})
  set(MACOSX_BUNDLE_GUI_IDENTIFIER ${IDENTIFIER})
  set(MACOSX_BUNDLE_ICON_FILE ../images/icons/${TARGET}.icns)

  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -mmacosx-version-min=10.9"
    CACHE STRING "Flags used by the compiler during all build types." FORCE)

  #set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fvisibility-inlines-hidden"
  #CACHE STRING "Flags used by the compiler during all build types." FORCE)

  set(CMAKE_OSX_SYSROOT "/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.12.sdk/"
    CACHE STRING "MacOSX10.12.sdk." FORCE)

  add_executable(${TARGET} MACOSX_BUNDLE
    ${${TARGET}-exec_SRCS}
    ${${TARGET}_nongui_SRCS}
    ${${TARGET}_gui_SRCS}
    ${${TARGET}_UIS_H}
    ${${TARGET}_QRC_CPP}
    ../images/icons/${TARGET}.icns
  )

  set_target_properties(${TARGET} PROPERTIES MACOSX_BUNDLE_ICON_FILE ${TARGET}.icns)

endif()


# Finally actually set the linking dependencies to the executable.
target_link_libraries(${TARGET}
  Qt6::Widgets
  Qt6::Xml
  Qt6::Svg
  Qt6::SvgWidgets
  Qt6::PrintSupport
)


#### Debugging stuff
message(STATUS required_target_link_libraries: ${required_target_link_libraries})

message(STATUS "Now printing all the include directories -I...")

get_property(dirs DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY INCLUDE_DIRECTORIES)
foreach(dir ${dirs})
  message(STATUS "included dir='${dir}'")
endforeach()

message(STATUS "Now printing all the linking directories -L...")

get_property(dirs DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY LINK_DIRECTORIES)
foreach(dir ${dirs})
  message(STATUS "link directory dir='${dir}'")
endforeach()

if(PROFILE)
  message(STATUS "Profiling is requested, adding linker -pg flag.")
  set (CMAKE_EXE_LINKER_FLAGS "-pg")
endif()

if(NOT APPLE)
  # We want to install the binary arch-dependent target in
  # specific situations. To have proper control, we define the arch
  # component.
  install(TARGETS ${TARGET}
    RUNTIME
    COMPONENT arch
    DESTINATION ${BIN_DIR})
endif()


message("")
message(STATUS "${BoldGreen}Finished configuring of ${TARGET}.${ColourReset}")
message("")

