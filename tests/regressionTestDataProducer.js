iter = 0;

resultDir = "/home/rusconi/devel/msxpertsuite/development/tests/results";


///////////////////////////////////
mainWindow.openMassSpectrometryFile("/home/rusconi/devel/msxpertsuite/development/tests/data/mzml/protein-mobility-waters-synapt-hdms.mzml");

++iter;
fileName = resultDir + "/rt-test-" + iter + ".res"

rtPlot = lastTicChromPlot;

rtPlot.exportPlotToFile(fileName);

++iter;
fileName = resultDir + "/rt-test-" + iter + ".res"

rtPlot.exportPlotToFile(fileName, 0.2, 0.6);

++iter;
fileName = resultDir + "/mz-test-" + iter + ".res"

rtPlot.integrateToMz();

msPlot = lastMassSpecPlot;

msPlot.exportPlotToFile(fileName);

++iter;
fileName = resultDir + "/mz-test-" + iter + ".res"

msPlot.exportPlotToFile(fileName, 2200, 2400);

++iter;
fileName = resultDir + "/dt-test-" + iter + ".res"

msPlot.integrateToDt(2000,2400);

dtPlot = lastDriftSpectrumPlot;

dtPlot.exportPlotToFile(fileName);

++iter;
fileName = resultDir + "/dt-test-" + iter + ".res"

dtPlot.exportPlotToFile(fileName, 7, 14);

// At this point, close the file.

rtPlot.shouldDestroyPlotWidget();

print("Done first series of test data production");

///////////////////////////////////
mainWindow.openMassSpectrometryFile("/home/rusconi/devel/msxpertsuite/development/tests/data/db/protein-mobility-waters-synapt-hdms.db")

++iter;
fileName = resultDir + "/rt-test-" + iter + ".res"

rtPlot = lastTicChromPlot;

rtPlot.exportPlotToFile(fileName);

++iter;
fileName = resultDir + "/rt-test-" + iter + ".res"

rtPlot.exportPlotToFile(fileName, 0.2, 0.6);

++iter;
fileName = resultDir + "/mz-test-" + iter + ".res"

rtPlot.integrateToMz();

msPlot = lastMassSpecPlot;

msPlot.exportPlotToFile(fileName);

++iter;
fileName = resultDir + "/mz-test-" + iter + ".res"

msPlot.exportPlotToFile(fileName, 2200, 2400);

++iter;
fileName = resultDir + "/dt-test-" + iter + ".res"

msPlot.integrateToDt(2000,2400);

dtPlot = lastDriftSpectrumPlot;

dtPlot.exportPlotToFile(fileName);

++iter;
fileName = resultDir + "/dt-test-" + iter + ".res"

dtPlot.exportPlotToFile(fileName, 7, 14);

// At this point, close the file.

rtPlot.shouldDestroyPlotWidget();

print("Done second series of test data production");

///////////////////////////////////
mainWindow.openMassSpectrometryFile("/home/rusconi/devel/msxpertsuite/development/tests/data/mzml/protein-microtofq-bruker.mzml")

++iter;
fileName = resultDir + "/rt-test-" + iter + ".res"

rtPlot = lastTicChromPlot;

rtPlot.exportPlotToFile(fileName);

++iter;
fileName = resultDir + "/rt-test-" + iter + ".res"

rtPlot.exportPlotToFile(fileName, 2, 6);

++iter;
fileName = resultDir + "/mz-test-" + iter + ".res"

rtPlot.integrateToMz();

msPlot = lastMassSpecPlot;

msPlot.exportPlotToFile(fileName);

++iter;
fileName = resultDir + "/mz-test-" + iter + ".res"

msPlot.exportPlotToFile(fileName, 900, 1500);

// At this point, close the file.

rtPlot.shouldDestroyPlotWidget();

print("Done third series of test data production");

///////////////////////////////////
mainWindow.openMassSpectrometryFile("/home/rusconi/devel/msxpertsuite/development/tests/data/db/protein-microtofq-bruker.db")

++iter;
fileName = resultDir + "/rt-test-" + iter + ".res"

rtPlot = lastTicChromPlot;

rtPlot.exportPlotToFile(fileName);

++iter;
fileName = resultDir + "/rt-test-" + iter + ".res"

rtPlot.exportPlotToFile(fileName, 2, 6);

++iter;
fileName = resultDir + "/mz-test-" + iter + ".res"

rtPlot.integrateToMz();

msPlot = lastMassSpecPlot;

msPlot.exportPlotToFile(fileName);

++iter;
fileName = resultDir + "/mz-test-" + iter + ".res"

msPlot.exportPlotToFile(fileName, 900, 1500);

// At this point, close the file.

rtPlot.shouldDestroyPlotWidget();

print("Done fourth series of test data production");


print("Finished production of test data.");

