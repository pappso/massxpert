
#include <iostream>
#include <ctime>
#include <random>

#include <catch.hpp>

#include <libmass/DataPoint.hpp>
#include <globals/globals.hpp>

#include <QDebug>
#include <QFile>

using std::cout;

#define TEST_DATA_DIR "/home/rusconi/devel/msxpertsuite/development/tests/data"

// qDebug() << __FILE__ << __LINE__ << __FUNCTION__
//<< "Should compile libmass tests.";

namespace msXpSlibmass
{

TEST_CASE("construct an initialized data point.", "[DataPoint]")
{

  DataPoint invalidDataPoint;
  REQUIRE(invalidDataPoint.isValid() == false);

  DataPoint datapoint1(12345.123456789, 987.987654321);

  REQUIRE(datapoint1.key() == 12345.123456789);
  REQUIRE(datapoint1.val() == 987.987654321);

  REQUIRE(datapoint1.isValid() == true);

  DataPoint datapoint2(datapoint1);

  REQUIRE(datapoint2.key() == 12345.123456789);
  REQUIRE(datapoint2.val() == 987.987654321);

  QString xyLine = "12345.123456789 987.987654321\n";

  DataPoint datapoint3(xyLine);
  REQUIRE(datapoint3.key() == 12345.123456789);
  REQUIRE(datapoint3.val() == 987.987654321);

  DataPoint datapoint4;

  bool res = datapoint4.initialize(xyLine);
  REQUIRE(res == true);
  REQUIRE(datapoint4.key() == 12345.123456789);
  REQUIRE(datapoint4.val() == 987.987654321);
}


// TEST_CASE("", "[DataPoint]")
//{

//}

} // namespace msXpSlibmass
