
#include <iostream>
#include <ctime>
#include <random>

#include <catch.hpp>

#include <libmass/MsMultiHash.hpp>
#include <libmass/MassSpectrum.hpp>

#include <globals/globals.hpp>

#include <QDebug>
#include <QFile>

using std::cout;

#define TEST_DATA_DIR "/home/rusconi/devel/msxpertsuite/development/tests/data"

// qDebug() << __FILE__ << __LINE__ << __FUNCTION__
//<< "Should compile libmass tests.";

namespace msXpSlibmass
{

TEST_CASE("Test the ms multi hash", "[MsMultiHash]")
{
  // Craft a series of spectra and simulate a profile acquisition. In the map,
  // insert new retention time mass spectrum pointer pairs.

  QStringList xyFormatStringList;
  xyFormatStringList << "694.5166264003 133.0000000000"
                     << "694.5333824250 195.0000000000"
                     << "694.5501386518 225.0000000000"
                     << "694.5668950807 220.0000000000"
                     << "694.5836517118 286.0000000000"
                     << "694.6004085450 332.0000000000"
                     << "694.6171655803 282.0000000000"
                     << "694.6339228177 164.0000000000"
                     << "694.6506802573 140.0000000000"
                     << "694.6674378989 156.0000000000"
                     << "694.6841957428 153.0000000000"
                     << "694.7009537887 89.0000000000"
                     << "694.7177120368 104.0000000000"
                     << "694.7344704869 143.0000000000"
                     << "694.7512291393 196.0000000000"
                     << "694.7679879937 295.0000000000"
                     << "694.7847470503 396.0000000000"
                     << "694.8015063090 399.0000000000"
                     << "694.8182657698 369.0000000000"
                     << "694.8350254327 345.0000000000";

  int xyFormatStringListSize = xyFormatStringList.size();
  QString xyFormatString     = xyFormatStringList.join("\n");
  MassSpectrum *massSpecIdx0 = new MassSpectrum;
  massSpecIdx0->initialize(xyFormatString);
  REQUIRE(xyFormatStringListSize == massSpecIdx0->size());

  MassSpectrum *massSpecIdx0bis = new MassSpectrum;
  massSpecIdx0bis->initialize(xyFormatString);
  REQUIRE(xyFormatStringListSize == massSpecIdx0bis->size());

  xyFormatStringList.clear();
  xyFormatStringList << "694.5166130065 186.0000000000"
                     << "694.5333690308 264.0000000000"
                     << "694.5501252573 288.0000000000"
                     << "694.5668816859 264.0000000000"
                     << "694.5836383166 276.0000000000"
                     << "694.6003951495 303.0000000000"
                     << "694.6171521845 267.0000000000"
                     << "694.6339094216 189.0000000000"
                     << "694.6506668608 170.0000000000"
                     << "694.6674245022 165.0000000000"
                     << "694.6841823457 173.0000000000"
                     << "694.7009403913 159.0000000000"
                     << "694.7176986390 180.0000000000"
                     << "694.7344570889 212.0000000000"
                     << "694.7512157409 245.0000000000"
                     << "694.7679745950 299.0000000000"
                     << "694.7847336512 357.0000000000"
                     << "694.8014929096 381.0000000000"
                     << "694.8182523701 343.0000000000"
                     << "694.8350120327 285.0000000000";

  xyFormatStringListSize     = xyFormatStringList.size();
  xyFormatString             = xyFormatStringList.join("\n");
  MassSpectrum *massSpecIdx1 = new MassSpectrum;
  massSpecIdx1->initialize(xyFormatString);
  REQUIRE(xyFormatStringListSize == massSpecIdx1->size());

  MassSpectrum *massSpecIdx1bis = new MassSpectrum;
  massSpecIdx1bis->initialize(xyFormatString);
  REQUIRE(xyFormatStringListSize == massSpecIdx1bis->size());

  xyFormatStringList.clear();
  xyFormatStringList << "694.5166130065 204.0000000000"
                     << "694.5333690308 277.0000000000"
                     << "694.5501252573 300.0000000000"
                     << "694.5668816859 286.0000000000"
                     << "694.5836383166 273.0000000000"
                     << "694.6003951495 276.0000000000"
                     << "694.6171521845 268.0000000000"
                     << "694.6339094216 235.0000000000"
                     << "694.6506668608 223.0000000000"
                     << "694.6674245022 195.0000000000"
                     << "694.6841823457 220.0000000000"
                     << "694.7009403913 233.0000000000"
                     << "694.7176986390 249.0000000000"
                     << "694.7344570889 247.0000000000"
                     << "694.7512157409 263.0000000000"
                     << "694.7679745950 299.0000000000"
                     << "694.7847336512 335.0000000000"
                     << "694.8014929096 354.0000000000"
                     << "694.8182523701 317.0000000000"
                     << "694.8350120327 249.0000000000";

  xyFormatStringListSize     = xyFormatStringList.size();
  xyFormatString             = xyFormatStringList.join("\n");
  MassSpectrum *massSpecIdx2 = new MassSpectrum;
  massSpecIdx2->initialize(xyFormatString);
  REQUIRE(xyFormatStringListSize == massSpecIdx2->size());

  MassSpectrum *massSpecIdx2bis = new MassSpectrum;
  massSpecIdx2bis->initialize(xyFormatString);
  REQUIRE(xyFormatStringListSize == massSpecIdx2bis->size());

  xyFormatStringList.clear();
  xyFormatStringList << "694.5165997465 215.0000000000"
                     << "694.5333557706 264.0000000000"
                     << "694.5501119967 292.0000000000"
                     << "694.5668684250 285.0000000000"
                     << "694.5836250554 296.0000000000"
                     << "694.6003818880 310.0000000000"
                     << "694.6171389226 304.0000000000"
                     << "694.6338961594 256.0000000000"
                     << "694.6506535983 232.0000000000"
                     << "694.6674112394 212.0000000000"
                     << "694.6841690826 235.0000000000"
                     << "694.7009271278 255.0000000000"
                     << "694.7176853753 284.0000000000"
                     << "694.7344438248 268.0000000000"
                     << "694.7512024765 272.0000000000"
                     << "694.7679613303 304.0000000000"
                     << "694.7847203862 316.0000000000"
                     << "694.8014796442 304.0000000000"
                     << "694.8182391044 288.0000000000"
                     << "694.8349987667 250.0000000000";

  xyFormatStringListSize     = xyFormatStringList.size();
  xyFormatString             = xyFormatStringList.join("\n");
  MassSpectrum *massSpecIdx3 = new MassSpectrum;
  massSpecIdx3->initialize(xyFormatString);
  REQUIRE(xyFormatStringListSize == massSpecIdx3->size());

  MassSpectrum *massSpecIdx3bis = new MassSpectrum;
  massSpecIdx3bis->initialize(xyFormatString);
  REQUIRE(xyFormatStringListSize == massSpecIdx3bis->size());

  xyFormatStringList.clear();
  xyFormatStringList << "694.5165997465 237.0000000000"
                     << "694.5333557706 261.0000000000"
                     << "694.5501119967 304.0000000000"
                     << "694.5668684250 312.0000000000"
                     << "694.5836250554 297.0000000000"
                     << "694.6003818880 279.0000000000"
                     << "694.6171389226 288.0000000000"
                     << "694.6338961594 268.0000000000"
                     << "694.6506535983 269.0000000000"
                     << "694.6674112394 238.0000000000"
                     << "694.6841690826 248.0000000000"
                     << "694.7009271278 242.0000000000"
                     << "694.7176853753 250.0000000000"
                     << "694.7344438248 256.0000000000"
                     << "694.7512024765 266.0000000000"
                     << "694.7679613303 276.0000000000"
                     << "694.7847203862 270.0000000000"
                     << "694.8014796442 269.0000000000"
                     << "694.8182391044 281.0000000000"
                     << "694.8349987667 262.0000000000";

  xyFormatStringListSize     = xyFormatStringList.size();
  xyFormatString             = xyFormatStringList.join("\n");
  MassSpectrum *massSpecIdx4 = new MassSpectrum;
  massSpecIdx4->initialize(xyFormatString);
  REQUIRE(xyFormatStringListSize == massSpecIdx4->size());

  MassSpectrum *massSpecIdx4bis = new MassSpectrum;
  massSpecIdx4bis->initialize(xyFormatString);
  REQUIRE(xyFormatStringListSize == massSpecIdx4bis->size());

  xyFormatStringList.clear();

  MsMultiHash msMultiHash;

  msMultiHash.insert(0, massSpecIdx0);
  msMultiHash.insert(0, massSpecIdx0bis);
  msMultiHash.insert(1, massSpecIdx1);
  msMultiHash.insert(1, massSpecIdx1bis);
  msMultiHash.insert(2, massSpecIdx2);
  msMultiHash.insert(2, massSpecIdx2bis);
  msMultiHash.insert(3, massSpecIdx3);
  msMultiHash.insert(3, massSpecIdx3bis);
  msMultiHash.insert(4, massSpecIdx4);
  msMultiHash.insert(4, massSpecIdx4bis);

  REQUIRE(msMultiHash.keys().size() == 10);
  REQUIRE(msMultiHash.uniqueKeys().size() == 5);

  REQUIRE(msMultiHash.values().size() == 10);

  int failedComparisons = 0;

  for(int iter = 0; iter < 5; ++iter)
    {
      if(msMultiHash.values(iter).size() != 2)
        ++failedComparisons;
    }

  REQUIRE(failedComparisons == 0);

  // Now test the key/value retrievals.

  QList<double> doubleList;

  msMultiHash.rangedKeys(doubleList, 1, 3);

  // The call above internally uses uniqueKeys(), so the result must be 3 and
  // not 5.
  REQUIRE(doubleList.size() == 3);

  REQUIRE(doubleList.contains(1));
  REQUIRE(doubleList.contains(2));
  REQUIRE(doubleList.contains(3));

  QList<MassSpectrum *> massSpectra;

  massSpectra.clear();

  int res = msMultiHash.rangedValues(massSpectra, 1, 3);

  // The function above returns the number of values that matched the key
  // range.
  REQUIRE(res == 6);

  REQUIRE(massSpectra.size() == 6);

  REQUIRE(massSpectra.contains(massSpecIdx1));
  REQUIRE(massSpectra.contains(massSpecIdx1bis));
  REQUIRE(massSpectra.contains(massSpecIdx2));
  REQUIRE(massSpectra.contains(massSpecIdx2bis));
  REQUIRE(massSpectra.contains(massSpecIdx3));
  REQUIRE(massSpectra.contains(massSpecIdx3bis));
}


} // namespace msXpSlibmass
