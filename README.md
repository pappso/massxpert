massXpert
=========

massXpert (of the http://www.msxpertsuite.org software collection) is a program
that allows one to perform the following tasks:

 - Make brand new polymer chemistry definitions using powerful grammar rules;

 - Use the polymer chemistry definitions to perform easily sophisticated
   calculations in a desktop calculator-like manner;

 - Perform sophisticated polymer chemistry simulations:

   - Edit polymer sequences;
   - Modify chemically the sequences, either in internal monomers or to the
   - polymer ends;
   - Cross-link monomers intra-sequence;
   - Perform digestions (chemical/enzymatic) of the sequences and simulate the
     oligomer produced thereof;
   - Perform gas-phase fragmentations of precursor oligomers and simulate the
     product-ion spectra;
   - Perform arbitrary mass searches in the polymer sequence;
   - Perform powerful peak list manipulations and comparisons.

massXpert is written in C++ and is portable.
