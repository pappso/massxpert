#############################################################
#############################################################
# CMake configuration
cmake_minimum_required(VERSION 3.12)

############################################################
############################################################
# Basic information about project

project(massXpert
  DESCRIPTION "A program to model and simulate mass spectra on (bio-)polymers"
  HOMEPAGE_URL "http://wwww.msxpertsuite.org/massxpert")

set(VERSION 7.0.0)


include(GNUInstallDirs)

set(HOME_DEVEL_DIR $ENV{HOME}/devel)
message("\n${BoldRed}The devel directory where all the development projects
should reside: ${HOME_DEVEL_DIR}.${ColourReset}\n")


# Add folder where are supportive functions
set(CMAKE_UTILS_PATH ${CMAKE_SOURCE_DIR}/CMakeStuff)
set(CMAKE_TOOLCHAINS_PATH ${CMAKE_UTILS_PATH}/toolchains)
set(CMAKE_MODULE_PATH ${CMAKE_UTILS_PATH}/modules)

# This include must come before all the others
# It must include the config-generated config.h file
# before the others.
include_directories(${CMAKE_BINARY_DIR})

# And now the source directory contains the .h|.hpp files for its .cpp files.
include_directories(${CMAKE_SOURCE_DIR})

# For the config.h file.
set(CMAKE_INCLUDE_CURRENT_DIR ON)

# Include the system's uname that fills in SYSTEM_UNAME_S.
# Sets WIN32 if SYSTEM_UNAME_S is "^.*MING64.*"
# Note that WIN32 is set even on 64 bits systems.
include(${CMAKE_UTILS_PATH}/systemUname.cmake)

# Include the various colors we want to use in the output
include(${CMAKE_UTILS_PATH}/outputColors.cmake)

set(CMAKE_COLOR_MAKEFILE ON)
set(CMAKE_VERBOSE_MAKEFILE ON)

message("\n${BoldRed}Configuring build for project ${CMAKE_PROJECT_NAME}${ColourReset}\n")

# This export will allow using the flags to be used by
# youcompleteme (vim plugin).
set(CMAKE_EXPORT_COMPILE_COMMANDS 1)

if(EXISTS "${CMAKE_CURRENT_BINARY_DIR}/compile_commands.json" )
  execute_process( COMMAND cmake -E copy_if_different
    ${CMAKE_CURRENT_BINARY_DIR}/compile_commands.json
    ${CMAKE_CURRENT_SOURCE_DIR}/compile_commands.json
    )
endif()


# We want C++17
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
message(STATUS "${BoldGreen}CMAKE_CXX_COMPILER: ${CMAKE_CXX_COMPILER}${ColourReset}")

#############################################################
# We do not want warnings for unknown pragmas:
message(STATUS "Setting definition -Wno-unknown-pragmas.${ColourReset}")
add_definitions(-Wno-unknown-pragmas)

# Enable warnings and possibly treat them as errors
message(STATUS "${BoldGreen}Setting definition -Wall -pedantic.${ColourReset}")
add_definitions(-Wall -pedantic)
message(STATUS "${BoldGreen}Setting definition -Wextra.${ColourReset}")
add_definitions(-Wextra)

if(WARN_AS_ERROR)
  message(STATUS "${BoldYellow}Setting definition -Werror.${ColourReset}")
  add_definitions(-Werror)
endif()

message("${BoldGreen}CMAKE_SOURCE_DIR: ${CMAKE_SOURCE_DIR}${ColourReset}")


#############################################################
#############################################################
# Platform-specific CMake configuration
if(WIN32 OR _WIN32)

  if(MXE)

    # Run the following cmake command line:
    # x86_64-w64-mingw32.shared-cmake -DCMAKE_BUILD_TYPE=Release -DMXE=1 ../../development
    include(${CMAKE_TOOLCHAINS_PATH}/mxe-toolchain.cmake)

    # Set the name to the systemUname variable because in this situation that name
    # is not found, it it passed as a flag in the command line.
    set(SYSTEM_UNAME_S "mxe")

  elseif(WIN10MINGW64)
    include(${CMAKE_TOOLCHAINS_PATH}/win10-mingw64-toolchain.cmake)
  endif()

elseif(UNIX AND NOT APPLE)

  include(${CMAKE_TOOLCHAINS_PATH}/unix-toolchain.cmake)

elseif(APPLE)

  include(${CMAKE_TOOLCHAINS_PATH}/apple-macport-toolchain.cmake)

endif()

message("")
message(STATUS "${BoldGreen}Starting configuration of ${CMAKE_PROJECT_NAME}${ColourReset}")
message("")
message(STATUS "${BoldYellow}The build toolchain is: ${SYSTEM_UNAME_S}${ColourReset}")
message("")


#############################################################
#############################################################
# Essential software configuration
message(STATUS "CMAKE_CURRENT_BINARY_DIR: " ${CMAKE_CURRENT_BINARY_DIR})

if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release CACHE STRING
    "Type of build, options are: None, Debug, Release, RelWithDebInfo, MinSizeRel."
    FORCE)
endif(NOT CMAKE_BUILD_TYPE)

if(CMAKE_BUILD_TYPE MATCHES "Release")
  message(STATUS "Compiling in release mode.")
  add_definitions("-DQT_NO_DEBUG_OUTPUT")
endif()

if(CMAKE_BUILD_TYPE MATCHES "Debug")
  message(STATUS "Compiling in debug mode.")
  message(STATUS "Add definition -ggdb3 to format debug output for GDB.")
  add_definitions(-ggdb3)
endif()

if(CMAKE_BUILD_TYPE MATCHES "RelWithDebInfo")
  message(STATUS "Compiling in release with debug info mode.")
endif( CMAKE_BUILD_TYPE MATCHES "RelWithDebInfo" )

message(STATUS "${BoldYellow}CMAKE_BUILD_TYPE: ${CMAKE_BUILD_TYPE}.${ColourReset}")


# Set additional project information
set(COMPANY "msXpertSuite.org")
set(COPYRIGHT "Copyright (c) 2008-2022 Filippo Rusconi. Licensed under GPLv3+")
set(IDENTIFIER "org.msxpertsuite")


if(PROFILE)
  message(STATUS "${BoldYellow}Profiling is requested, adding -pg flag.${ColourReset}")
  add_definitions(-pg)
endif()

message(STATUS "${BoldYellow}Main CMAKE_BINARY_DIR: ${CMAKE_BINARY_DIR}${ColourReset}")
message(STATUS "${BoldYellow}CMAKE_CXX_COMPILER: ${CMAKE_CXX_COMPILER}${ColourReset}")


# It is essential to run these config steps before delving into the minexpert
# directory, because at that moment they will need what are
# going to prepare right now.
CONFIGURE_FILE(${CMAKE_SOURCE_DIR}/CMakeStuff/splashscreen.svg.in
  ${CMAKE_SOURCE_DIR}/images/svg/splashscreen.svg @ONLY)

# Make the conversion of the svg file into a png, but only on GNU/Linux
if(UNIX AND NOT APPLE)
  execute_process(COMMAND convert ${CMAKE_SOURCE_DIR}/images/svg/splashscreen.svg
    ${CMAKE_SOURCE_DIR}/images/splashscreen.png)
endif()

#############################################################
######################
# BUILD OF THE PROGRAM

message(STATUS "Adding subdirectory src for TARGET: ${TARGET}")
add_subdirectory(src)

####################################################
# Installation directories depending on the platform
####################################################

# See the CMakeStuff/toolchains platform-specific files.
message(STATUS "CMAKE_INSTALL_PREFIX: ${CMAKE_INSTALL_PREFIX}")
message(STATUS "BIN_DIR: ${BIN_DIR}")
message(STATUS "DATA_DIR: ${DATA_DIR}")
message(STATUS "DOC_DIR: ${DOC_DIR}")


#############################################################
###################
# BUILD OF THE DATA
message(STATUS "Adding subdirectory data for project: ${CMAKE_PROJECT_NAME}")
add_subdirectory(data)


#############################################################
################################
# BUILD OF THE DOC / USER MANUAL
message(STATUS "Adding subdirectory doc for project: ${CMAKE_PROJECT_NAME}")
add_subdirectory(doc)


# Use all the configured paths to create the config.h file.
# The config file is binary directory-specific !!!
CONFIGURE_FILE(${CMAKE_SOURCE_DIR}/CMakeStuff/config.h.in
  ${CMAKE_CURRENT_BINARY_DIR}/src/config.h @ONLY)


#############################################################
# summary
message("")
message(STATUS "${BoldRed}~~~~~~~~~~~~~~~~~~~~~~~~ SUMMARY ~~~~~~~~~~~~~~~~~~~~~~~~${ColourReset}")
message(STATUS "${BoldRed}   General configuration of the msXpertSuite project ${ColourReset}")
message("")

message(STATUS "${BoldYellow}System is detected by CMake as ${SYSTEM_UNAME_S}${ColourReset}")
message(STATUS "${BoldYellow}CMAKE_CXX_COMPILER: ${CMAKE_CXX_COMPILER}${ColourReset}")
message(STATUS "${BoldYellow}CMAKE_CXX_STANDARD: ${CMAKE_CXX_STANDARD}${ColourReset}")

message("")

message(STATUS "${BoldYellow}CMAKE_INSTALL_PREFIX: ${CMAKE_INSTALL_PREFIX}${ColourReset}")
message(STATUS "${BoldYellow}TARGET: ${TARGET}${ColourReset}")
message(STATUS "${BoldYellow}BIN_DIR: ${BIN_DIR}${ColourReset}")
message(STATUS "${BoldYellow}DOC_DIR: ${DOC_DIR}${ColourReset}")

if(PROFILE)
  message(STATUS "${BoldYellow}Profiling is requested (-DPROFILE=1)${ColourReset}")
else()
  message(STATUS "${BoldYellow}Profiling is NOT requested (-DPROFILE=0)${ColourReset}")
endif()

if(WARNASERR)
  message(STATUS "${BoldYellow}Warnings ARE treated as errors (-DWARNASERR=1)${ColourReset}")
else()
  message(STATUS "${BoldYellow}Warnings NOT treated as errors (-DWARNASERR=0)${ColourReset}")
endif()

if(CMAKE_BUILD_TYPE STREQUAL "Debug")
  message(STATUS "${BoldYellow}Debug support IS requested.${ColourReset}")
else()
  message(STATUS "${BoldYellow}Debug support NOT requested.${ColourReset}")
endif()

message("")
message(STATUS "${BoldYellow}The typical cmake invocation on Debian GNU/Linux would be: cmake -DCMAKE_BUILD_TYPE=Debug -DBUILD_USER_MANUAL=1 ../../development${ColourReset}")
message(STATUS "${BoldYellow}The typical cmake invocation on win10-mingw64 would be: cmake -DCMAKE_BUILD_TYPE=Release  ../../development${ColourReset}") 
message(STATUS "${BoldYellow}The typical cmake invocation on MXE would be: x86_64-w64-mingw32.shared-cmake -DCMAKE_BUILD_TYPE=Release -DMXE=1 ../../development${ColourReset}") 
message("")

message("")
message(STATUS "${BoldRed}~~~~~~~~~~~~~~~~~~~~~~~~ SUMMARY ~~~~~~~~~~~~~~~~~~~~~~~~${ColourReset}")
message("")






































































