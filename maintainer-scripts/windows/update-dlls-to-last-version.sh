#/bin/bash

srcDir="/mingw64/bin"
destDir="/c/msXpertSuite-libDeps"

echo "destDir: ${destDir}"

echo "Copying dll files from ${srcDir} to ${destDir}"
echo "OK ? yY|nN"

read answer

if [ ${answer} != "y" ] && [ ${answer} != "Y" ] 
then
	echo "Operation aborted by the user."
fi

wasError="false"

for file in ${destDir}/*.dll
do
	echo "Current file: ${file}"

	baseName=$(basename ${file})
	echo "baseName: ${baseName}"

	if [ ! -e ${srcDir}/${baseName} ]
	then
		echo "Attention, file ${baseName} not found."
	else
		cp -v ${srcDir}/${baseName} ${destDir}

		if [ $? != 0 ]
		then
			echo "Failed to copy ${baseName}"
			wasError="true"
		fi

	fi
done

# And now the platforms/qwindows.dll
cp -v /mingw64/share/qt5/plugins/platforms/qwindows.dll ${destDir}/platforms

if [ "$?" != "0" ]
then
	echo "Attention, failed to copy platforms/qwindows.dll."
	exit 1
fi

# And now the plugins

for file in ${destDir}/plugins/imageformats/*.dll
do
	cp -v /mingw64/share/qt5/plugins/imageformats/$(basename ${file}) ${destDir}/plugins/imageformats
done

for file in ${destDir}/plugins/printsupport/*.dll
do
	cp -v /mingw64/share/qt5/plugins/printsupport/$(basename ${file}) ${destDir}/plugins/printsupport
done

for file in ${destDir}/plugins/sqldrivers/*.dll
do
	cp -v /mingw64/share/qt5/plugins/sqldrivers/$(basename ${file}) ${destDir}/plugins/sqldrivers
done

if [ ${wasError} == "true" ]
then
	echo "There was at least one error."
	exit 1
fi

echo "Done."
exit 0

