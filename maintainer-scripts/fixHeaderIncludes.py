#!/usr/bin/python3

import glob,os

sedCommandList = []

for root, dirs, files in os.walk(os.getcwd()):
  for file in files:
    if file.endswith(".hpp"):
      #print(os.path.join(root, file))
      #print(file)
      dirName = root.replace('/home/rusconi/devel/msxpertsuite/development/', '')

      sedCommand = []
      sedCommand.append('''sed -i 's|include "''')
      sedCommand.append(file)
      sedCommand.append('''"|include <''')
      sedCommand.append(dirName)
      sedCommand.append( '''/''')
      sedCommand.append(file)
      sedCommand.append('''>|\' ${file}''');

      #print(''.join(sedCommand))

      sedCommandList.append(sedCommand)

prefix = '''for file in $(find -name "*.[ch]pp"); do '''
suffix = '''; done'''

for commandList in sedCommandList:
  print(prefix + ''.join(commandList) + suffix)








