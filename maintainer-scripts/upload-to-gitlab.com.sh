#! /bin/zsh

zmodload zsh/zutil


usage()
{
  cat << EOF
  Usage:
        $0 --token <token> (vs2FYr4VdYZhTx-Wg2Xt gitlab or XrjajV_pL_1tugzjdjs2 forgemia) --file <file_name> --projectid <projectid> (23442125 gitlab or 3239 forgemia) --version <version> --repos <forgemia | gitlab>
EOF
}

if [ $1 = "--help" ] || [ $1 = "-h" ]
then
  usage
exit 0
fi

zparseopts -A opts -token: -file: -projectid: -version: -repos:

#echo "token: $opts[--token], file: $opts[--file], projectid: $opts[--projectid], version: $opts[--version], repos: $opts[--repos]"

packageradix=massXpert

repos="$opts[--repos]"

if [ -z "${repos}" ]
then
  print "Please provide a --repos \"<value>\", typically gitlab or forgemia\n"
  usage
  exit 1
fi

if [ ${repos} != "gitlab" ] && [ ${repos} != "forgemia" ] 
then
  print "Please provide a --repos \"<value>\", typically gitlab or forgemia\n"
  usage
  exit 1
fi

printf "Repos: ${repos}\n"

reposurl=""
token=""

if [ ${repos} = "gitlab" ] 
then
  token="vs2FYr4VdYZhTx-Wg2Xt"
  reposurl="https://gitlab.com/api/v4/projects"
fi

if [ ${repos} = "forgemia" ] 
then
  token="K6Q1KhykFUr8bNEiVyx9"
  reposurl="https://forgemia.inra.fr/api/v4/projects"
fi

if [ ! -z "$opts[--token]" ]
then
  token=$opts[--token]
fi

projectid=""

if [ ${repos} = "gitlab" ] 
then
  projectid="23431674"
fi

if [ ${repos} = "forgemia" ] 
then
  projectid="3239"
fi

if [ ! -z "$opts[--projectid]" ]
then
  projectid="$opts[--projectid]"
fi


file="$opts[--file]"
filebasename=$(basename ${file})

if [ -z "${file}" ]
then
  print "Please provide a --file \"<file_name>\" to upload, typically massxpert_7.2.0.orig.tar.gz \n"
  usage
  exit 1
fi


version="$opts[--version]"

if [ -z "${version}" ]
then
  print "Please provide a --version \"<value>\", typically 7.4.0.\n"
  usage
  exit 1
fi


printf "\nCommand to run: curl --verbose --header \"PRIVATE-TOKEN: ${token}\" --upload-file ${file} \"${reposurl}/${projectid}/packages/generic/${packageradix}/${version}/${filebasename}\"\n"

curl --verbose --header "PRIVATE-TOKEN: ${token}" --upload-file ${file} "${reposurl}/${projectid}/packages/generic/${packageradix}/${version}/${filebasename}"

printf "\nCommand that was run: curl --verbose --header \"PRIVATE-TOKEN: ${token}\" --upload-file ${file} \"${reposurl}/${projectid}/packages/generic/${packageradix}/${version}/${filebasename}\"\n"

